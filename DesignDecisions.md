# Design Decisions




Since this is a simple page with an API call to a 'external' site, I considered .NET Core inappropriate, since all of the API calls would need to be done from 'our' local server instead of directly from the client.  Unless we moved the API call out to the client (i.e ajax), in which case we have the equivalant of the Vue app.  So, although I have never used Vue before, a single-file Vue solution seemed cleaner.

I got it working with stubbed in data, but when I added in the API call, I encountered a CORS exception from the server.  A quick workaround was to use a third-party reverse proxy, but this is definitely not a production solution.  The proper solution depends on the planned hosting environment: if this page will be hosted on the the same server using a relative reference ('/api/accounts/getall'), or the local server can be added to the  CORS allowed origin list, then we can dispense with the reverse proxy.  Otherwise, we need to set up our own server to handle the API call; essentially our own reverse proxy.  Therefore, since we have our server handling all of the requests anyway, perhaps just using ASP.NET Core to begin with is the better solution.

Anyway, what I have at this point is a single-file solution that runs entirely in the browser, no web server required (other than the API); it can run from your local hard drive.  Obviously, this is not sustainable in a larger app and I have sprinkled comments in the code with suggestions for refactoring.

Also, I noticed that the spec calls for the phone number to be formatted with a hyphen following the area code, instead of the traditional space.  I wasn't sure if that was intentional.  I have implemented per spec, but with a comment to confirm, with the alternate code also as a comment.
